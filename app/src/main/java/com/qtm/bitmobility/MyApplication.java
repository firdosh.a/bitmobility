package com.qtm.bitmobility;

import android.app.Application;

import com.splunk.mint.Mint;

/**
 * Created by qtm-satish on 11/12/15.
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Mint.initAndStartSession(this, "efaf071e");
    }
}

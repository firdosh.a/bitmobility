package com.qtm.bitmobility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import utils.General;

public class Webview extends Activity {

    WebView web_app;
    private static ValueCallback<Uri> mUploadMessage;
    private final static int FILECHOOSER_RESULTCODE = 1;
    private ProgressBar progressBar;
    int i = 0;
    private ValueCallback<Uri[]> mFilePathCallback;
    private String mCameraPhotoPath;
    private static final int INPUT_FILE_REQUEST_CODE = 1;
    private Uri mCapturedImageURI = null;

    // PrintJob printJob;

    private void createWebPrintJob(final WebView webView) {
        i++;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                // webView.clearHistory();
                webView.post(new Runnable() {
                    @Override
                    public void run() {

                        PrintManager printManager = (PrintManager) getSystemService(Context.PRINT_SERVICE);
                        // Give the job a name.
                        String jobName = getString(R.string.app_name)
                                + " Document";

                        // ------ as it was-------------------//

                        PrintDocumentAdapter printAdapter = webView
                                .createPrintDocumentAdapter();

                        printManager.print(jobName, printAdapter,
                                new PrintAttributes.Builder().build());

                        // new PrintAttributes.Builder().build()
                        // ----- end--//

                        // Start a print job, passing a printDocumentAdapter as
                        // argument to handle the generation of a print document

                        // ****** created printadapter....in adapter folder
                        // PrintAdapter printDoc = new
                        // PrintAdapter(Webview.this);
                        //
                        //
                        // printManager.print(jobName, printDoc, null);

                        // ---------- example whole bye google code file

                        // MyPrintDocumentAdapter mdoc = new
                        // MyPrintDocumentAdapter(
                        // Webview.this);
                        // printManager.print(
                        // jobName,
                        // mdoc,
                        // new PrintAttributes.Builder().setMediaSize(
                        // PrintAttributes.MediaSize.ISO_A4)
                        // .build());

                        // ---------google code file end

                        // Get a PrintManager instance
                        // PrintManager printManager = (PrintManager)
                        // getSystemService(Context.PRINT_SERVICE);
                        //
                        // // Get a print adapter instance
                        // PrintDocumentAdapter printAdapter = webView
                        // .createPrintDocumentAdapter();
                        //
                        // // Create a print job with name and adapter instance
                        // ,,=
                        // // getString(R.string.app_name) + " Document"+
                        // String jobName = getString(R.string.app_name)
                        // + " Document" + i;
                        // PrintJob printJob = printManager.print(jobName,
                        // printAdapter,
                        // new PrintAttributes.Builder().build());

                        // @SuppressWarnings("unused")
                        // = new PrintJobInfo;
                        // // = printManager.print(
                        // // jobName,
                        // // printAdapter,
                        // // new PrintAttributes.Builder().setMediaSize(
                        // // PrintAttributes.MediaSize.ISO_A4)
                        // // .build());
                        // //
                        // PrintAttributes patrb = new PrintAttributes.Builder()
                        // .setMediaSize(PrintAttributes.MediaSize.ISO_A0)
                        // .build();
                        //
                        // printManager.print(jobName, printAdapter,
                        // new PrintAttributes.Builder().build());
                        //
                        // printManager.print("Razor HMTL Hybrid",
                        // webView.createPrintDocumentAdapter(), null);

                        Log.e("TAG", "createWebPrintJob called");
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // PrintJobInfo info = printJob.getInfo();
                // Log.i("Print job info", String.valueOf(info));
            }
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_webview);

        web_app = (WebView) findViewById(R.id.web_app);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        web_app.getSettings().setJavaScriptEnabled(true);
        web_app.getSettings().setLoadWithOverviewMode(true);
        web_app.getSettings().setAllowFileAccess(true);
        web_app.getSettings().setJavaScriptEnabled(true);
        web_app.getSettings().setAppCacheEnabled(false);
        web_app.setDownloadListener(new CustomDownloadListener());
        WebSettings webSettings = web_app.getSettings();
        webSettings.setSupportZoom(true);
        web_app.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        web_app.addJavascriptInterface(new WebAppInterface(), "Android");
        web_app.setWebChromeClient(new CustomWebViewClient());
        // web_app.loadData(
        // "<html><body><a href=\"javascript:Android.Print();\">print</a></body></html>",
        // "text/html", "UTF-8");

        SharedPreferences prefs = getSharedPreferences("PREFERENCE",
                MODE_PRIVATE);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String string_url = "";
        if (getIntent().hasExtra("link")) {
            string_url = getIntent().getStringExtra("link");
            string_url = string_url + "&deviceid=" + preferences.getString(General.DEVICE_ID, "") + "&devicetype=1" + "&isNotif=1";
        } else {
            string_url = prefs.getString("URLPREF",
                    "default_value_here_if_string_is_missing");
        }

        Log.e("string_url", "" + string_url);
        if (string_url.equals("default_value_here_if_string_is_missing")) {
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean("FirstRun", true); // set to has run

            edit.commit();

            Intent i = new Intent(Webview.this, MainActivity.class);

            startActivity(i);
        } else if (string_url.startsWith("http://")) {
            web_app.loadUrl(string_url);

        } else {
            web_app.loadUrl("http://" + string_url);
        }

    }

    class CustomDownloadListener implements DownloadListener {
        public void onDownloadStart(final String url, String userAgent,
                                    String contentDisposition, String mimetype, long contentLength) {

            //new DownloadAsyncTask(getApplicationContext()).execute(url);
            // web_app.loadUrl(url);
            Intent intentUrl = new Intent(Webview.this, PdfWebview.class);
            intentUrl.putExtra("pdf_url",url);
            startActivity(intentUrl);
        }

    }

    private class DownloadAsyncTask extends AsyncTask<String, Integer, String> {

        private NotificationHelper mNotificationHelper;
        private String file_name = "";
        private String file_url = "";

        public DownloadAsyncTask(Context context) {
            mNotificationHelper = new NotificationHelper(context);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

            mNotificationHelper.createNotification();

        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // TODO Auto-generated method stub
            mNotificationHelper.progressUpdate(progress[0]);
            super.onProgressUpdate(progress);
        }

        @Override
        protected String doInBackground(String... arg0) {
            String result = "";
            String url = arg0[0];
            file_url = url;
            for (int i = 10; i <= 100; i += 10) {
                try {
                    Thread.sleep(1000);
                    publishProgress(i);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (Environment.getExternalStorageState().equals(
                    Environment.MEDIA_MOUNTED)) {
                HttpClient httpClient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);
                InputStream inputStream = null;
                try {
                    HttpResponse httpResponse = httpClient.execute(httpGet);

                    BufferedHttpEntity bufferedHttpEntity = new BufferedHttpEntity(
                            httpResponse.getEntity());

                    inputStream = bufferedHttpEntity.getContent();

                    String fileName = android.os.Environment
                            .getExternalStorageDirectory().getAbsolutePath()
                            + "/E_quarry";
                    File directory = new File(fileName);
                    File file = new File(directory, url.substring(url
                            .lastIndexOf("/")));
                    file_name = url.substring(url.lastIndexOf("/")+1);
                    directory.mkdirs();

                    FileOutputStream fileOutputStream = new FileOutputStream(
                            file);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int len = 0;

                    while (inputStream.available() > 0
                            && (len = inputStream.read(buffer)) != -1) {
                        byteArrayOutputStream.write(buffer, 0, len);
                    }

                    fileOutputStream.write(byteArrayOutputStream.toByteArray());
                    fileOutputStream.flush();

                    result = file.getAbsolutePath();
                } catch (Exception ex) {
                    Log.e(Webview.class.toString(), ex.getMessage(), ex);
                    result = ex.getClass().getSimpleName() + " "
                            + ex.getMessage();
                } finally {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException ex) {

                            result = ex.getClass().getSimpleName() + " "
                                    + ex.getMessage();
                        }
                    }
                }
            } else {
                result = "No Sd Card";
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            AlertDialog.Builder builder = new AlertDialog.Builder(Webview.this);
            builder.setMessage(result).setPositiveButton(("Ok"), null)
                    .setTitle("Done!");
            builder.show();

            mNotificationHelper.completed();

            if(!TextUtils.isEmpty(file_name)) {
                Uri uri = Uri.fromFile(new File(result));

                try {
                    Intent intentUrl = new Intent(Intent.ACTION_VIEW);
                    intentUrl.setDataAndType(uri, "application/pdf");
                    intentUrl.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentUrl);
//                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(file_url));
//                    startActivity(browserIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(Webview.this, "No PDF Viewer Installed", Toast.LENGTH_LONG).show();
                }
            }

        }

    }

    class CustomWebViewClient extends WebChromeClient {
        // the current WebView will handle the url

        // For Android 3.0+
        @Override
        public void onProgressChanged(WebView view, int progress) {
            // TODO Auto-generated method stub
            progressBar.setProgress(0);
            FrameLayout progressBarLayout = (FrameLayout) findViewById(R.id.progressBarLayout);
            progressBarLayout.setVisibility(View.VISIBLE);
            Webview.this.setProgress(progress * 1000);

            progressBar.incrementProgressBy(progress);

            if (progress == 100) {
                progressBarLayout.setVisibility(View.GONE);
            }
        }

        // For Android 5.0
        public boolean onShowFileChooser(WebView view, ValueCallback<Uri[]> filePath, WebChromeClient.FileChooserParams fileChooserParams) {
            // Double check that we don't have any existing callbacks
            if (mFilePathCallback != null) {
                mFilePathCallback.onReceiveValue(null);
            }
            mFilePathCallback = filePath;
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                    takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath);
                } catch (IOException ex) {
                    // Error occurred while creating the File
                    Log.e("WebView", "Unable to create Image File", ex);
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    mCameraPhotoPath = "file:" + photoFile.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                            Uri.fromFile(photoFile));
                } else {
                    takePictureIntent = null;
                }
            }
            Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
            contentSelectionIntent.setType("image/*");
            Intent[] intentArray;
            if (takePictureIntent != null) {
                intentArray = new Intent[]{takePictureIntent};
            } else {
                intentArray = new Intent[0];
            }
            Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
            chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
            chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
            startActivityForResult(chooserIntent, INPUT_FILE_REQUEST_CODE);
            return true;
        }
        // openFileChooser for Android 3.0+
        public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
            mUploadMessage = uploadMsg;
            // Create AndroidExampleFolder at sdcard
            // Create AndroidExampleFolder at sdcard
            File imageStorageDir = new File(
                    Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES)
                    , "AndroidExampleFolder");
            if (!imageStorageDir.exists()) {
                // Create AndroidExampleFolder at sdcard
                imageStorageDir.mkdirs();
            }
            // Create camera captured image file path and name
            File file = new File(
                    imageStorageDir + File.separator + "IMG_"
                            + String.valueOf(System.currentTimeMillis())
                            + ".jpg");
            mCapturedImageURI = Uri.fromFile(file);
            // Camera capture image intent
            final Intent captureIntent = new Intent(
                    android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
            i.addCategory(Intent.CATEGORY_OPENABLE);
            i.setType("image/*");
            // Create file chooser intent
            Intent chooserIntent = Intent.createChooser(i, "Image Chooser");
            // Set camera intent to file chooser
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS
                    , new Parcelable[] { captureIntent });
            // On select image call onActivityResult method of activity
            startActivityForResult(chooserIntent, FILECHOOSER_RESULTCODE);
        }
        // openFileChooser for Android < 3.0
        public void openFileChooser(ValueCallback<Uri> uploadMsg) {
            openFileChooser(uploadMsg, "");
        }
        //openFileChooser for other Android versions
        public void openFileChooser(ValueCallback<Uri> uploadMsg,
                                    String acceptType,
                                    String capture) {
            openFileChooser(uploadMsg, acceptType);
        }
    }

//        public void openFileChooser(ValueCallback<Uri> uploadMsg,
//                                    String acceptType) {
//            Webview.mUploadMessage = uploadMsg;
//            Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//
//            // i.setType("file/*");
//            i.putExtra("CONTENT_TYPE", "*/*");
//            i.addCategory(Intent.CATEGORY_DEFAULT);
//            Webview.this.startActivityForResult(
//                    Intent.createChooser(i, "File Chooser"),
//                    Webview.FILECHOOSER_RESULTCODE);
//        }
//
//        public void openFileChooser(ValueCallback<Uri> uploadMsg,
//                                    String acceptType, String capture) {
//            this.openFileChooser(uploadMsg, "");
//        }
//
//        // For Android < 3.0
//        public void openFileChooser(ValueCallback<Uri> uploadMsg) {
//            this.openFileChooser(uploadMsg, "");
//        }

//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data)
    {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode != INPUT_FILE_REQUEST_CODE || mFilePathCallback == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }
            Uri[] results = null;
            // Check that the response is a good one
            if (resultCode == Activity.RESULT_OK) {
                if (data == null) {
                    // If there is not data, then we may have taken a photo
                    if (mCameraPhotoPath != null) {
                        results = new Uri[]{Uri.parse(mCameraPhotoPath)};
                    }
                } else {
                    String dataString = data.getDataString();
                    if (dataString != null) {
                        results = new Uri[]{Uri.parse(dataString)};
                    }
                }
            }
            mFilePathCallback.onReceiveValue(results);
            mFilePathCallback = null;
        } else if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            if (requestCode != FILECHOOSER_RESULTCODE || mUploadMessage == null) {
                super.onActivityResult(requestCode, resultCode, data);
                return;
            }
            if (requestCode == FILECHOOSER_RESULTCODE) {
                if (null == this.mUploadMessage) {
                    return;
                }
                Uri result = null;
                try {
                    if (resultCode != RESULT_OK) {
                        result = null;
                    } else {
                        // retrieve from the private variable if the intent is null
                        result = data == null ? mCapturedImageURI : data.getData();
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "activity :" + e,
                            Toast.LENGTH_LONG).show();
                }
                mUploadMessage.onReceiveValue(result);
                mUploadMessage = null;
            }
        }
        return;

//        if (requestCode == FILECHOOSER_RESULTCODE) {
//            if (null == mUploadMessage)
//                return;
//            Uri result = intent == null || resultCode != RESULT_OK ? null
//                    : intent.getData();
//            mUploadMessage.onReceiveValue(result);
//            mUploadMessage = null;
//
//        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return imageFile;
    }

    // -- Printing class

    private class WebAppInterface {

        /**
         * Show a toast from the web page
         */

        @JavascriptInterface
        public void Print() {

            Toast.makeText(getApplicationContext(), "Print Function",
                    Toast.LENGTH_LONG).show();
            if (web_app != null) {
                Log.e("TAG", "WebAppInterface.Print called");
                createWebPrintJob(web_app);

            }

        }
    }
}

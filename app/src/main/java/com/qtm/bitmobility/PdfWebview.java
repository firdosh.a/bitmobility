package com.qtm.bitmobility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

/**
 * Created by qtm-a-android on 18/2/16.
 */
public class PdfWebview extends Activity {

    WebView web_app;

    private ProgressBar progressBar;
    int i = 0;
    private String pdf_url;
    private ImageView back;
    // PrintJob printJob;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pdfwebview);

         pdf_url = getIntent().getStringExtra("pdf_url");

        web_app = (WebView) findViewById(R.id.web_app);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
        back = (ImageView) findViewById(R.id.back);

        web_app.getSettings().setJavaScriptEnabled(true);
        //web_app.getSettings().setLoadWithOverviewMode(true);
        //web_app.getSettings().setAllowFileAccess(true);
        //web_app.getSettings().setAppCacheEnabled(false);
        WebSettings webSettings = web_app.getSettings();
        webSettings.setSupportZoom(true);
        web_app.setWebChromeClient(new CustomWebViewClient());
//        web_app.setWebViewClient(new WebViewClient() {
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                return false;
//            }
//        });

        web_app.loadUrl("https://docs.google.com/gview?embedded=true&url="+pdf_url);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    class CustomWebViewClient extends WebChromeClient {
        // the current WebView will handle the url

        // For Android 3.0+
        @Override
        public void onProgressChanged(WebView view, int progress) {
            // TODO Auto-generated method stub
            progressBar.setProgress(0);
            FrameLayout progressBarLayout = (FrameLayout) findViewById(R.id.progressBarLayout);
            progressBarLayout.setVisibility(View.VISIBLE);
            PdfWebview.this.setProgress(progress * 1000);

            progressBar.incrementProgressBy(progress);

            if (progress == 100) {
                progressBarLayout.setVisibility(View.GONE);
            }
        }
    }
}


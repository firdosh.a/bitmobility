package com.qtm.bitmobility;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		/*
         * boolean firstrun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
		 * .getBoolean("firstrun", true);
		 */

        SharedPreferences runCheck = getSharedPreferences("PREFERENCE", 0);
        // if (firstrun) {
        boolean hasrun = runCheck.getBoolean("FirstRun", true);
        if (hasrun) {

            Intent i = new Intent(MainActivity.this,
                    First_Screen_Activity.class);
            startActivity(i);
            finish();

            // Save the state

        } else {

            Intent i = new Intent(MainActivity.this, Webview.class);
            startActivity(i);
            finish();

        }

    }


}

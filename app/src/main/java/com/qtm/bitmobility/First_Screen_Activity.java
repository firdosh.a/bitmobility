package com.qtm.bitmobility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import utils.General;

public class First_Screen_Activity extends Activity {
    Button btn_go;
    EditText edt_url;
    TextView txt_register, txt_phone, txt_company, txturl, txtmo;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar mRegistrationProgressBar;
    private SharedPreferences preferences;
    private static final String TAG = "First_Screen_Activity";

    private String link;

    //AIzaSyDQWnCDNlwCmn8sZs5S3Kk3YUaYxnUbhYg
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        getDataFromIntent(getIntent());
        btn_go.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String entered_url = "";
//                        edt_url.getText().toString() + "/?deviceid=" + preferences.getString(General.DEVICE_ID, "") + "&devicetype=1";
                Log.e(TAG, " String All : " + entered_url);

                if (edt_url.getText().toString().equals("")) {
                    edt_url.setError("Please Enter Web site Url");
                } else if (!(Patterns.WEB_URL.matcher(edt_url.getText().toString() + "/?deviceid=" + preferences.getString(General.DEVICE_ID, "") + "&devicetype=1").matches())) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            First_Screen_Activity.this);
                    builder.setMessage("Please Enter Valid Web site Url")
                            .setPositiveButton(("Ok"), null).setTitle("Alert");
                    builder.show();
                } else {

                    if (!entered_url.startsWith("http://")) {
                        entered_url = edt_url.getText().toString() + "/?deviceid=" + preferences.getString(General.DEVICE_ID, "") + "&devicetype=1";
                    }

                    SharedPreferences settings = getSharedPreferences("PREFERENCE", 0);
                    SharedPreferences.Editor edit = settings.edit();
                    edit.putBoolean("FirstRun", false); // set to has run
                    edit.putString("URLPREF", entered_url);
                    edit.commit();

                    Intent i = new Intent(First_Screen_Activity.this, Webview.class);
                    startActivity(i);
                    finish();


                }
//                String all = edt_url.getText().toString() + "/?deviceid=" + preferences.getString(General.DEVICE_ID, "") + "&devicetype=1";
//                Log.e(TAG, " String All : " + all);
            }
        });
    }

    private void getDataFromIntent(Intent intent) {
        if (intent.hasExtra("link")) {
            link = intent.getStringExtra("link");

            if (!TextUtils.isEmpty(link)) {
                edt_url.setText(link);
                Intent i = new Intent(First_Screen_Activity.this, Webview.class);
                i.putExtra("link", link);
                startActivity(i);
                finish();
            }
        }
    }

    void init() {
        try {
            btn_go = (Button) findViewById(R.id.btn_go);
            edt_url = (EditText) findViewById(R.id.edt_url);
            txt_company = (TextView) findViewById(R.id.txt_company);
            txt_phone = (TextView) findViewById(R.id.txt_phone);
            txt_register = (TextView) findViewById(R.id.txt_register);
            txturl = (TextView) findViewById(R.id.txturl);

            Typeface font = Typeface.createFromAsset(getApplicationContext()
                    .getAssets(), "OPENSANS-REGULAR.TTF");
            edt_url.setTypeface(font);
            txt_company.setTypeface(font);
            txturl.setTypeface(font);
//            txtmo.setTypeface(font);
            txt_phone.setTypeface(font);
            txt_register.setTypeface(font);

            preferences = PreferenceManager.getDefaultSharedPreferences(First_Screen_Activity.this);

            mRegistrationProgressBar = (ProgressBar) findViewById(R.id.registrationProgressBar);
            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                    SharedPreferences sharedPreferences =
                            PreferenceManager.getDefaultSharedPreferences(context);
                    boolean sentToken = sharedPreferences
                            .getBoolean(General.SENT_TOKEN_TO_SERVER, false);
                    if (sentToken) {
                        Log.e(TAG, " device_id :  " + preferences.getString(General.DEVICE_ID, ""));
                    } else {

                    }
                }
            };

            if (checkPlayServices()) {
                // Start IntentService to register this application with GCM.
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(General.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, General.PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

}

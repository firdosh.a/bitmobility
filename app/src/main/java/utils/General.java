package utils;

/**
 * Created by qtm-quantum on 25/11/15.
 */
public class General {
    public static final String SENDER_ID = "866207104251";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String NOTIFICATION_COUNTER = "app_notification_counter";

    public static String DEVICE_ID = "device_id";
}

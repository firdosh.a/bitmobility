package Adapter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.graphics.pdf.PdfDocument.Page;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.CancellationSignal.OnCancelListener;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.pdf.PrintedPdfDocument;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;

public class PrintAdapter_Custom extends PrintDocumentAdapter {
	private PrintedPdfDocument mPdfDocument;
	private boolean mCancelled;
	public static final String LOG_TAG = "PrintActivity";
	private final Object mLock = new Object();
	Context context;

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	public PrintAdapter_Custom(Context context) {
		this.context = context;
	}

	@Override
	public void onLayout(PrintAttributes oldAttributes,
			PrintAttributes newAttributes,
			CancellationSignal cancellationSignal,
			LayoutResultCallback callback, Bundle metadata) {

		mPdfDocument = new PrintedPdfDocument(context, newAttributes);

		final boolean cancelled;
		synchronized (mLock) {
			mCancelled = false;
			cancelled = mCancelled;
		}
		if (cancelled) {
			mPdfDocument.close();
			mPdfDocument = null;
			callback.onLayoutCancelled();
		} else {
			PrintDocumentInfo info = new PrintDocumentInfo.Builder(
					"print_view.pdf")
					.setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
					.setPageCount(1).build();
			callback.onLayoutFinished(info, false);
		}
		cancellationSignal.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel() {
				Log.i(LOG_TAG, "onLayout#onCancel()");
				synchronized (mLock) {
					mCancelled = true;
					mLock.notifyAll();
				}
			}
		});

	}

	@Override
	public void onWrite(final PageRange[] pages,
			final ParcelFileDescriptor destination,
			final CancellationSignal canclleationSignal,
			final WriteResultCallback callback) {
		//View view = context.getContentView();
		Log.i(LOG_TAG, "onWrite[pages: " + Arrays.toString(pages) + "]");
		final SparseIntArray writtenPagesArray = new SparseIntArray();
		final AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
			@Override
			protected void onPreExecute() {
				synchronized (mLock) {
					for (int i = 0; i < 1; i++) {
						if (isCancelled()) {
							mPdfDocument.close();
							mPdfDocument = null;
							callback.onWriteCancelled();
							break;
						}
						if (containsPage(pages, i)) {
							writtenPagesArray.append(writtenPagesArray.size(),
									i);
							Page page = mPdfDocument.startPage(i);
						//	view.draw(page.getCanvas());
							mPdfDocument.finishPage(page);
						}
					}
				}
			}

			@Override
			protected Void doInBackground(Void... params) {
				try {
					mPdfDocument.writeTo(new FileOutputStream(destination
							.getFileDescriptor()));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				mPdfDocument.close();
				mPdfDocument = null;
				List<PageRange> pageRanges = new ArrayList<PageRange>();
				int start = -1;
				int end = -1;
				final int writtenPageCount = writtenPagesArray.size();
				for (int i = 0; i < writtenPageCount; i++) {
					if (start < 0) {
						start = writtenPagesArray.valueAt(i);
					}
					int oldEnd = end = start;
					while (i < writtenPageCount && (end - oldEnd) <= 1) {
						oldEnd = end;
						end = writtenPagesArray.valueAt(i);
						i++;
					}
					PageRange pageRange = new PageRange(start, end);
					pageRanges.add(pageRange);
					start = end = -1;
				}
				PageRange[] writtenPages = new PageRange[pageRanges.size()];
				pageRanges.toArray(writtenPages);
				callback.onWriteFinished(new PageRange[] { PageRange.ALL_PAGES });
				return null;
			}
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void[]) null);
		canclleationSignal.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel() {
				synchronized (mLock) {
					Log.i(LOG_TAG, "onWrite#onCancel()");
					task.cancel(true);
					mLock.notifyAll();
				}
			}
		});
	}

	@Override
	public void onFinish() {
		// TODO Auto-generated method stub
		super.onFinish();
	}

	private boolean containsPage(PageRange[] pageRanges, int page) {
		final int pageRangeCount = pageRanges.length;
		for (int i = 0; i < pageRangeCount; i++) {
			if (pageRanges[i].getStart() <= page
					&& pageRanges[i].getEnd() >= page) {
				return true;
			}
		}
		return false;
	}

}

package Adapter;

import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.graphics.pdf.PdfDocument.Page;
import android.graphics.pdf.PdfDocument.PageInfo;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintAttributes.MediaSize;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.pdf.PrintedPdfDocument;

public class PrintAdapter extends PrintDocumentAdapter {
	Context context;
	private int pageHeight;
	private int pageWidth;
	//public PdfDocument myPdfDocument;
	private PrintedPdfDocument myPdfDocument;
	public int totalpages = 4;

	public PrintAdapter(Context context) {
		this.context = context;
	}

	// ====================

	@Override
	public void onLayout(PrintAttributes oldAttributes,
			PrintAttributes newAttributes,
			CancellationSignal cancellationSignal,
			LayoutResultCallback callback, Bundle metadata) {

		// Creates a new PDFdocument with the right attributes
		myPdfDocument = new PrintedPdfDocument(context, newAttributes);

		pageHeight = newAttributes.getMediaSize().getHeightMils() / 1000 * 72;
		pageWidth = newAttributes.getMediaSize().getWidthMils() / 1000 * 72;

		// Responds to Cancellation request
		if (cancellationSignal.isCanceled()) {
			callback.onLayoutCancelled();
			return;
		}

		// Returns print information to printing framework
		if (totalpages > 0) {
			PrintDocumentInfo.Builder builder = new PrintDocumentInfo.Builder(
					"print_output.pdf").setContentType(
					PrintDocumentInfo.CONTENT_TYPE_DOCUMENT).setPageCount(
					totalpages);

			PrintDocumentInfo info = builder.build();
			callback.onLayoutFinished(info, true);
		} else {
			callback.onLayoutFailed("Page count is zero.");
		}
	}

	@Override
	public void onWrite(final PageRange[] pageRanges,
			final ParcelFileDescriptor destination,
			final CancellationSignal cancellationSignal,
			final WriteResultCallback callback) {

		for (int i = 0; i < 1; i++) {
			if (pageInRange(pageRanges, i)) {
			//	PageInfo newPage = myPdfDocument.startPage(i);

				Page page = myPdfDocument.startPage(i);
				if (cancellationSignal.isCanceled()) {
					callback.onWriteCancelled();
					myPdfDocument.close();
					myPdfDocument = null;
					return;
				}
				drawPage(page, i);
				myPdfDocument.finishPage(page);
			}
		}

		try {
			myPdfDocument.writeTo(new FileOutputStream(destination
					.getFileDescriptor()));
		} catch (IOException e) {
			callback.onWriteFailed(e.toString());
			return;
		} finally {
			myPdfDocument.close();
			myPdfDocument = null;
		}

		callback.onWriteFinished(pageRanges);
	}

	private boolean pageInRange(PageRange[] pageRanges, int page) {
		for (int i = 0; i < pageRanges.length; i++) {
			if ((page >= pageRanges[i].getStart())
					&& (page <= pageRanges[i].getEnd()))
				return true;
		}
		return false;
	}

	private void drawPage(PdfDocument.Page page, int pagenumber) {
		Canvas canvas = page.getCanvas();

		pagenumber++; // Make sure page numbers start at 1

		int titleBaseLine = 72;
		int leftMargin = 54;

		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setTextSize(40);
		canvas.drawText("Test Print Document Page " + pagenumber, leftMargin,
				titleBaseLine, paint);

		paint.setTextSize(14);
		canvas.drawText(
				"This is some test content to verify that custom document printing works",
				leftMargin, titleBaseLine + 35, paint);

		if (pagenumber % 2 == 0)
			paint.setColor(Color.RED);
		else
			paint.setColor(Color.GREEN);

		PageInfo pageInfo = page.getInfo();

		canvas.drawCircle(pageInfo.getPageWidth() / 2,
				pageInfo.getPageHeight() / 2, 150, paint);
	}

	@SuppressWarnings("unused")
	private int computePageCount(PrintAttributes printAttributes) {
		int itemsPerPage = 4; // default item count for portrait mode

		MediaSize pageSize = printAttributes.getMediaSize();
		if (!pageSize.isPortrait()) {
			// Six items per page in landscape orientation
			itemsPerPage = 6;
		}

		// Determine number of print items
		int printItemCount = 4;

		return (int) Math.ceil(printItemCount / itemsPerPage);
	}

	// =====================

	/**
	 * Layout method.
	 */
	// @Override
	// public void onLayout(PrintAttributes oldAttributes,
	// PrintAttributes newAttributes,
	// CancellationSignal cancellationSignal,
	// LayoutResultCallback callback,
	// Bundle metadata) {
	//
	// // Creates a new PDFdocument with the right attributes
	// myPdfDocument = new PrintedPdfDocument(context, newAttributes);
	//
	// pageHeight =
	// newAttributes.getMediaSize().getHeightMils()/1000 * 72;
	// pageWidth =
	// newAttributes.getMediaSize().getWidthMils()/1000 * 72;
	//
	// // Responds to Cancellation request
	// if (cancellationSignal.isCanceled() ) {
	// callback.onLayoutCancelled();
	// return;
	// }
	//
	// // Returns print information to printing framework
	// if (totalpages > 0) {
	// PrintDocumentInfo.Builder builder = new PrintDocumentInfo
	// .Builder("print_output.pdf")
	// .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
	// .setPageCount(totalpages);
	//
	// PrintDocumentInfo info = builder.build();
	// callback.onLayoutFinished(info, true);
	// } else {
	// callback.onLayoutFailed("Page count is zero.");
	// Toast.makeText(context, "Faildddd", Toast.LENGTH_LONG).show();
	// }
	// }
	//
	// /*
	// * Called by the printing framework
	// *
	// */
	// @Override
	// public void onWrite(final PageRange[] pageRanges,
	// final ParcelFileDescriptor destination,
	// final CancellationSignal cancellationSignal,
	// final WriteResultCallback callback) {
	//
	// for (int i = 0; i < totalpages; i++) {
	// if (pageInRange(pageRanges, i))
	// {
	// PageInfo newPage = new PageInfo.Builder(pageWidth,
	// pageHeight, i).create();
	//
	// PdfDocument.Page page = myPdfDocument.startPage(newPage);
	//
	// if (cancellationSignal.isCanceled()) {
	// callback.onWriteCancelled();
	// myPdfDocument.close();
	// myPdfDocument = null;
	// return;
	// }
	// drawPage(page, i);
	// myPdfDocument.finishPage(page);
	// }
	// }
	//
	// try {
	// myPdfDocument.writeTo(new FileOutputStream(
	// destination.getFileDescriptor()));
	// } catch (IOException e) {
	// callback.onWriteFailed(e.toString());
	// return;
	// } finally {
	// myPdfDocument.close();
	// myPdfDocument = null;
	// }
	//
	// callback.onWriteFinished(pageRanges);
	// }
	//
	// private boolean pageInRange(PageRange[] pageRanges, int page)
	// {
	// for (int i = 0; i<pageRanges.length; i++)
	// {
	// if ((page >= pageRanges[i].getStart()) &&
	// (page <= pageRanges[i].getEnd()))
	// return true;
	// }
	// return false;
	// }
	//
	//
	// private void drawPage(PdfDocument.Page page,
	// int pagenumber) {
	// Canvas canvas = page.getCanvas();
	//
	// pagenumber++; // Make sure page numbers start at 1
	//
	// int titleBaseLine = 72;
	// int leftMargin = 54;
	//
	// Paint paint = new Paint();
	// paint.setColor(Color.BLACK);
	// paint.setTextSize(40);
	// canvas.drawText(
	// "Test Print Document Page " + pagenumber,
	// leftMargin,
	// titleBaseLine,
	// paint);
	//
	// paint.setTextSize(14);
	// canvas.drawText("This is some test content to verify that custom document printing works",
	// leftMargin, titleBaseLine + 35, paint);
	//
	// if (pagenumber % 2 == 0)
	// paint.setColor(Color.RED);
	// else
	// paint.setColor(Color.GREEN);
	//
	// PageInfo pageInfo = page.getInfo();
	//
	//
	// canvas.drawCircle(pageInfo.getPageWidth()/2,
	// pageInfo.getPageHeight()/2,
	// 150,
	// paint);
	// }

	// @Override
	// public void onWrite(PageRange[] pageRanges, ParcelFileDescriptor
	// destination,
	// CancellationSignal cancellationSignal, WriteResultCallback callback) {
	//
	// // First we need to loop over all of our pages and draw their content
	// for(int index = 0; index < totalpages; index ++) {
	// // If at any point while drawing the pages we see that the cancellation
	// signal has
	// // been triggered we make the onWriteCancelled method call, clean-up the
	// // PrintedPdfDocument and return
	// if (cancellationSignal.isCanceled()) {
	// callback.onWriteCancelled();
	// myPdfDocument.close();
	// myPdfDocument = null;
	// return;
	// }
	//
	// // It is possible that the requested pageRanges do not include every
	// page, in this
	// // case we need to potentially skip some pages
	// if (!pageRangesContainPage(pageRanges, index)) {
	// continue;
	// }
	//
	// // Use the PrintedPdfDocument object to start a page
	// PdfDocument.Page page = myPdfDocument.startPage(index);
	// Canvas canvas = page.getCanvas();
	//
	// // … draw some stuff to your page’s canvas…
	//
	// // … you may also want to write some PageRange information here so you
	// can
	// // determine all of the ranges you supplied pages for when you go to call
	// // onWriteFinished
	//
	// myPdfDocument.finishPage(page);
	// }
	//
	// // Next we need to write out the updated PrintedPdfDocument to the
	// specified
	// // destination
	// try {
	// myPdfDocument.writeTo(new
	// FileOutputStream(destination.getFileDescriptor()));
	// } catch(IOException e) {
	// callback.onWriteFailed(e.toString());
	// return;
	// } finally {
	// // Whether or not the write is successful we need to clean-up the
	// // PrintedPdfDocument object
	// mPdfDocument.close();
	// mPdfDocument = null;
	// }
	//
	// // It is possible that we did not supply pages for all of the requested
	// PageRanges so we
	// // need to pass onWriteFinished an accurate array of the PageRanges. In
	// most cases it
	// // would be sufficient to pass back the same PageRanges array object that
	// was passed
	// // in to us, pageRanges.
	// PageRange[] completedPages = getCompletedPageRanges();
	// callback.onWriteFinished(completedPages);
	// }

}